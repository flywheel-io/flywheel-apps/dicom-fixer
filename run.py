#!/usr/bin/env python
"""The run script."""

import logging
import sys
import typing as t
from pathlib import Path

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dicom_fixer import main as dicom_fixer
from fw_gear_dicom_fixer import parser

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> t.Optional[int]:  # pragma: no cover
    """Parse config and run.

    Args:
        context (GearToolkitContext): The gear context.

    """
    # Parse config
    (
        dicom,
        transfer_syntax,
        unique,
        zip_single,
        new_uids_needed,
        fail_status,
        convert_palette,
        pixel_data_check,
    ) = parser.parse_config(context)

    # Get seeds for generating UIDs if requested
    # Use acquisition and session labels as seeds
    # for generating new UIDs to ensure new UIDs are
    # deterministic and unique across sessions.
    uid_seeds = None
    if new_uids_needed:
        uid_seeds = get_acquisition_session_subject_labels(context)

    # Run gear
    out_fname, events = dicom_fixer.run(
        dicom,
        Path(context.output_dir),
        transfer_syntax=transfer_syntax,
        unique=unique,
        zip_single=zip_single,
        convert_palette=convert_palette,
        new_uids_needed=new_uids_needed,
        uid_seeds=uid_seeds,
        pixel_data_check=pixel_data_check,
    )
    # Add metadata
    file_ = context.get_input("dicom")
    assert file_
    # NOTE: Result will be `PASS` if either, no fixes were attempted (no output)
    # or fixes were attempted and successfully written to output
    #
    # Result will be `FAIL` if fixes were attempted and were not able to
    # be written, or if `fail_status` is True.
    # `fail_status` may be True if the user configured transfer_syntax=True
    # and the parser changed it to False due to memory constraints, or if
    # decompression was attempted and an AttributeError was raised,
    # or if there was an error reading the pixel data.
    no_tag = False
    if fail_status:
        # If fail_status is true here, gear failed at parser size check
        fail_reason = [
            "File too large for decompression, standardize_transfer_syntax "
            "switched to False"
        ]
        if not events:
            # Initialize events dict to hold gear fail
            events = {}
        if events.get("Gear Fail"):
            events.get("Gear Fail").extend(fail_reason)
        else:
            events.update({"Gear Fail": fail_reason})
    if events and any(event.get("tag") == "Gear Fail" for event in events):
        # If other Gear Fail conditions were passed via events,
        # make sure to update fail_status
        fail_status = True

        # Check if 'Pixel data unparsable' exists within the corresponding 'Gear Fail' events
        if any(
            event.get("event") == "Pixel data unparsable"
            and event.get("tag") == "Gear Fail"
            for event in events
        ):
            no_tag = True
            log.warning(
                "Because pixel-data-check failed, file will not be tagged, "
                "so that any gear rule pipelines related to this tag will "
                "not be triggered."
            )

    context.metadata.add_qc_result(
        file_,
        "fixed",
        state=("PASS" if events is not None and not fail_status else "FAIL"),
        events=(events or []),  # Ensure events is a list, not a dictionary
    )

    # Tag file as long as pixel-data-check did not fail.
    # If pixel-data-check was selected and failed, don't tag the file to
    # interrupt downstream gear rules.
    if not no_tag:
        tag = context.config.get("tag")
        context.metadata.add_file_tags(file_, t.cast(str, tag))

    # Set output file type to dicom.
    context.metadata.update_file(out_fname, type="dicom")

    # If fail_status is True, the gear should return as unsuccessful,
    # because either standardize_transfer_syntax or pixel_data_check failed.
    if fail_status:
        # Extract all relevant 'Gear Fail' events from the list of dictionaries.
        fail_reasons = "\n".join(
            event["event"] for event in events if event.get("tag") == "Gear Fail"
        )
        log.warning(
            "The dicom-fixer job completed, however a gear fail condition was met; "
            "therefore, this job will be marked as unsuccessful. Fail reason(s): \n"
            f"{fail_reasons}"
        )
        return 1


def get_acquisition_session_subject_labels(
    context: GearToolkitContext,
) -> t.Dict[str, t.List[str]]:
    """Get the list of acquisition.label, session.label, and subject.label of input DICOM file.

    Args:
        context (GearToolkitContext): The gear context.

    Returns:
        t.Dict[str, t.List[str]]: entries 'acquisition.label',
        'session.label', and 'subject.label', with the values as lists
    """
    file_dict = context.get_input_file_object("dicom")
    file_ = context.client.get_file(file_dict.get("file_id"))
    acquisition_ = context.get_parent(file_)
    session_ = context.get_parent(acquisition_)
    subject_ = context.get_parent(session_)
    return {
        "acq.label": acquisition_.label,
        "ses.label": session_.label,
        "sub.label": subject_.label,
    }


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext(fail_on_validation=False) as gear_context:
        gear_context.init_logging()
        status = main(gear_context)

    sys.exit(status)
