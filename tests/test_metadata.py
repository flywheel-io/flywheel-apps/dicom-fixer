import logging
from importlib import metadata

import pytest
from fw_file.dicom.reader import ReplaceEvent
from pydicom.dataset import Dataset, FileDataset
from pydicom.tag import TupleTag

from fw_gear_dicom_fixer.metadata import (
    add_implementation,
    add_missing_uid,
    generate_and_set_new_uid,
    get_unique_UID,
    update_modified_dicom_info,
)


def test_add_implementation(create_dcm_fixture):
    dcm = create_dcm_fixture()

    add_implementation(dcm)
    fw_file_version = metadata.version("fw-file")
    assert dcm.ImplementationClassUID == "2.16.840.1.114570.4.1." + fw_file_version
    assert dcm.ImplementationVersionName.startswith("FWFILE")


def test_get_unique_UID_series(dcmcoll):
    """Test get_unique_UID returns SeriesInstanceUID."""
    dcms = dcmcoll(
        **{"test": {"SeriesInstanceUID": "123"}, "test2": {"SeriesInstanceUID": "123"}}
    )
    uid_tag = "SeriesInstanceUID"
    uid = get_unique_UID(dcms, uid_tag)
    assert uid == "123"


def test_get_unique_UID_empty(dcmcoll):
    """Test get_unique_UID returns empty SeriesInstanceUID."""
    dcms = dcmcoll(
        **{"test": {"SeriesInstanceUID": ""}, "test2": {"SeriesInstanceUID": ""}}
    )
    uid_tag = "SeriesInstanceUID"
    uid = get_unique_UID(dcms, uid_tag)
    assert uid == ""


def test_get_unique_UID_err_invalid_uid_tag(dcmcoll):
    """Test get_unique_UID throws error with invalid UID tag."""
    dcms = dcmcoll(
        **{"test": {"SeriesInstanceUID": "123"}, "test2": {"SeriesInstanceUID": "123"}}
    )
    with pytest.raises(SystemExit):
        get_unique_UID(dcms, "SOPInstanceUID")


def test_get_unique_UID_err_multiple_uids(dcmcoll):
    """Test get_unique_UID throws error with different SeriesInstanceUIDs."""
    dcms = dcmcoll(
        **{"test": {"SeriesInstanceUID": "123"}, "test2": {"SeriesInstanceUID": ""}}
    )
    with pytest.raises(SystemExit):
        get_unique_UID(dcms, "SeriesInstanceUID")


def test_add_missing_uid_series(mocker, dcmcoll):
    """Test add_missing_uid with SeriesInstanceUID."""
    dcms = dcmcoll(
        **{"test": {"SeriesInstanceUID": ""}, "test2": {"SeriesInstanceUID": ""}}
    )
    mock_get_unique_UID = mocker.patch(
        "fw_gear_dicom_fixer.metadata.get_unique_UID", return_value=""
    )
    mod = add_missing_uid(dcms)
    mock_get_unique_UID.assert_called_with(dcms, "SeriesInstanceUID")
    assert all([dcm.get("SeriesInstanceUID") for dcm in dcms])
    assert mod


def test_add_missing_uid_err_series(dcmcoll):
    """Test add_missing_uid throws error with different SeriesInstanceUIDs."""
    dcms = dcmcoll(
        **{"test": {"SeriesInstanceUID": "123"}, "test2": {"SeriesInstanceUID": ""}}
    )
    with pytest.raises(SystemExit):
        add_missing_uid(dcms)


def test_add_missing_uid_sop_uid(dcmcoll):
    """Test add_missing_uid with SOPInstanceUID."""
    dcms = dcmcoll(**{"test": {"SOPInstanceUID": ""}, "test2": {"SOPInstanceUID": ""}})
    mod = add_missing_uid(dcms)
    assert all([dcm.get("SOPInstanceUID") for dcm in dcms])
    assert mod


def test_generate_and_set_new_uid_series(mocker, dcmcoll):
    """Test generate_and_set_new_uid with SeriesInstanceUID."""
    dcms = dcmcoll(
        **{"test": {"SeriesInstanceUID": "123"}, "test2": {"SeriesInstanceUID": "123"}}
    )
    uid_tag = "SeriesInstanceUID"
    seed = ["sub 1", "ses 1", "acq 1"]
    mock_get_unique_UID = mocker.patch(
        "fw_gear_dicom_fixer.metadata.get_unique_UID", return_value="123"
    )
    mock_generate_uid = mocker.patch(
        "fw_gear_dicom_fixer.metadata.generate_uid", return_value="1.2.345"
    )
    mod = generate_and_set_new_uid(dcms, uid_tag, seed)
    mock_get_unique_UID.assert_called_with(dcms, uid_tag)
    mock_generate_uid.assert_called_with(entropy_srcs=["".join(seed)])
    assert all([dcm.get("SeriesInstanceUID") for dcm in dcms])
    assert all(
        [
            dcm.get("SeriesInstanceUID") == dcms[0].get("SeriesInstanceUID")
            for dcm in dcms
        ]
    )
    assert mod


def test_generate_and_set_new_uid_err_invalid_tag(dcmcoll):
    """Test generate_and_set_new_uid throws error with invalid UID tag."""
    dcms = dcmcoll(
        **{"test": {"SeriesInstanceUID": "123"}, "test2": {"SeriesInstanceUID": "123"}}
    )
    uid_tag = "SOPInstanceUID"
    seed = ["sub 1", "ses 1", "acq 1"]
    with pytest.raises(SystemExit):
        generate_and_set_new_uid(dcms, uid_tag, seed)


def test_generate_and_set_new_uid_study(mocker, dcmcoll):
    """Test generate_and_set_new_uid with StudyInstanceUID."""
    dcms = dcmcoll(
        **{"test": {"StudyInstanceUID": "123"}, "test2": {"StudyInstanceUID": "123"}}
    )
    uid_tag = "StudyInstanceUID"
    seed = ["sub 1", "ses 1"]
    mock_get_unique_UID = mocker.patch(
        "fw_gear_dicom_fixer.metadata.get_unique_UID", return_value="123"
    )
    mod = generate_and_set_new_uid(dcms, uid_tag, seed)
    mock_get_unique_UID.assert_called_with(dcms, uid_tag)
    assert all([dcm.get("StudyInstanceUID") for dcm in dcms])
    assert all(
        [dcm.get("StudyInstanceUID") == dcms[0].get("StudyInstanceUID") for dcm in dcms]
    )
    assert mod


# NOTE: Fails currently, will be fixed by GEAR-4304
@pytest.mark.xfail
def test_update_from_tracker_conforming(mocker, raw_data_elem, create_dcm_fixture):
    dcm = create_dcm_fixture()
    tracked = raw_data_elem(b"3000.0", tracked=True)
    tracked._replace(value=b"3.0")
    dcm.dataset.raw = FileDataset(None, Dataset(), file_meta=Dataset())
    dcm.read_context.dataelem_dict[hash(tracked)] = tracked
    update_modified_dicom_info(dcm)
    oas = dcm.dataset.raw.OriginalAttributesSequence
    assert len(oas) == 1
    assert len(oas[0].ModifiedAttributesSequence) == 1
    assert oas[0].ModifiedAttributesSequence[0].MagneticFieldStrength.real == 3000.0


def test_update_from_tracker_non_conforming(mocker, raw_data_elem, create_dcm_fixture):
    dcm = create_dcm_fixture()
    tracked = raw_data_elem(b"3000.0", tracked=True)
    tracked._replace(value=b"3.0")
    validate = mocker.patch("fw_file.dicom.reader.validate_value")
    validate.side_effect = ValueError("test")
    dcm.dataset.raw = FileDataset(None, Dataset(), file_meta=Dataset())
    dcm.read_context.dataelem_dict[hash(tracked)] = tracked
    update_modified_dicom_info(dcm)
    oas = dcm.dataset.raw.OriginalAttributesSequence
    nas = oas[0].NonconformingModifiedAttributesSequence
    assert len(nas) == 1
    assert nas[0].NonconformingDataElementValue == b"3000.0"
    assert nas[0].SelectorAttribute == TupleTag((0x0018, 0x0087))


# NOTE: Fails currently, will be fixed by GEAR-4304
@pytest.mark.xfail
def test_update_from_event_conforming(create_dcm_fixture):
    dcm = create_dcm_fixture()
    dcm.dataset.raw = FileDataset(None, Dataset(), file_meta=Dataset())
    replace = ReplaceEvent("MagneticFieldStrength", 3000.0, 3)
    update_modified_dicom_info(dcm, evts=[replace])
    oas = dcm.dataset.raw.OriginalAttributesSequence
    assert len(oas) == 1
    assert len(oas[0].ModifiedAttributesSequence) == 1
    assert oas[0].ModifiedAttributesSequence[0].MagneticFieldStrength == 3000.0


def test_update_from_event_non_conforming(create_dcm_fixture):
    dcm = create_dcm_fixture()
    dcm.dataset.raw = FileDataset(None, Dataset(), file_meta=Dataset())
    replace = ReplaceEvent("PatientSex", "male", "M")
    update_modified_dicom_info(dcm, evts=[replace])
    oas = dcm.dataset.raw.OriginalAttributesSequence
    nas = oas[0].NonconformingModifiedAttributesSequence
    assert len(nas) == 1
    assert nas[0].NonconformingDataElementValue == b"male"
    assert nas[0].SelectorAttribute == TupleTag((0x0010, 0x0040))


def test_update_from_event_nonexisting_tag(create_dcm_fixture, caplog):
    dcm = create_dcm_fixture()
    dcm.dataset.raw = FileDataset(None, Dataset(), file_meta=Dataset())
    replace = ReplaceEvent("NonExistingTag", "male", "M")
    with caplog.at_level(logging.DEBUG):
        update_modified_dicom_info(dcm, evts=[replace])
    "OriginalAttributesSequence" not in dcm.dataset.raw
    local_recs = [
        r for r in caplog.record_tuples if r[0] == "fw_gear_dicom_fixer.metadata"
    ]
    assert "Tag and VR not found" in local_recs[-1][2]
