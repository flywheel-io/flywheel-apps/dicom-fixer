"""Module to test main.py"""

import warnings
from pathlib import Path
from unittest.mock import MagicMock

import pytest
from fw_file import dicom

from fw_gear_dicom_fixer.main import get_output_filename, run


def test_run_new_uids_needed_true(mocker, dcmcoll, tmp_path, raw_data_elem):
    """Test run with new_uids_needed=True and Modality=US."""
    # coll = dcmcoll(**{"test1": {}})
    coll = dcmcoll(
        **{
            "test1": {"Modality": "US", "PhotometricInterpretation": "PALETTE COLOR"},
            "test2": {"Modality": "US", "PhotometricInterpretation": "PALETTE COLOR"},
        }
    )
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll
    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = True
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = False
    uid_seeds = {
        "acq.label": "acq 1",
        "ses.label": "ses 1",
        "sub.label": "sub 1",
    }

    def side_effect(*args, **kwargs):
        p_i = args[0].PhotometricInterpretation
        return f"Replace {p_i} -> RGB 8-bit"

    convert_color_space_fixer_mock = mocker.patch(
        "fw_gear_dicom_fixer.fixers.convert_color_space_fixer"
    )
    convert_color_space_fixer_mock.side_effect = side_effect

    _, evts = run(
        dicom_path=tmp_path,
        out_dir=tmp_path,
        transfer_syntax=True,
        unique=False,
        zip_single="no",
        convert_palette=True,
        new_uids_needed=True,
        pixel_data_check=False,
        uid_seeds=uid_seeds,
    )

    assert evts is not None

    assert any(
        evt["tag"] == "SeriesInstanceUID"
        and evt["event"]
        == "Replace SeriesInstanceUID: 1.2 -> 2.16.840.1.114570.4.2.449093468911739102522960519486247187173347"
        for evt in evts
    ), "Expected event for SeriesInstanceUID not found."

    assert any(
        evt["tag"] == "StudyInstanceUID"
        and evt["event"]
        == "Replace StudyInstanceUID: 1 -> 2.16.840.1.114570.4.2.448944703218591244650070189078351226017352"
        for evt in evts
    ), "Expected event for StudyInstanceUID not found."

    assert any(
        evt["tag"] == "PhotometricInterpretation"
        and evt["event"] == "Updated to Replace PALETTE COLOR -> RGB 8-bit"
        for evt in evts
    ), "Expected event for PhotometricInterpretation not found."


def test_run_from_zip_validation_mode_2(
    mocker, dcmcoll, tmp_path, raw_data_elem, caplog, config
):
    config.reading_validation_mode = 2
    p_sex = mocker.patch("fw_gear_dicom_fixer.fixers.fix_patient_sex")
    p_sex.side_effect = lambda _: warnings.warn("test")
    num_dcms = 100
    coll = dcmcoll(**{f"test-{i}": {} for i in range(num_dcms)})
    for i, dcm in enumerate(coll):
        tracked_elem1 = raw_data_elem(b"3000.0", tracked=True)
        tracked_elem1._replace(value=b"3.0")
        invalid = f"1.2.0{i}"
        valid = f"1.2.{i}"
        tracked_elem2 = raw_data_elem(
            bytes(invalid, encoding="ascii"),
            tag=(0x0008, 0x0018),
            tracked=True,
            VR="UI",
        )
        tracked_elem2._replace(value=bytes(valid, encoding="ascii"))
        # Mock tracker.track() to return what was passed in
        dcm.read_context.dataelem_dict[hash(tracked_elem1)] = tracked_elem1
        dcm.read_context.dataelem_dict[hash(tracked_elem2)] = tracked_elem2
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll
    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = True
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = False
    mock_generate_and_set_new_uid = mocker.patch(
        "fw_gear_dicom_fixer.metadata.generate_and_set_new_uid"
    )
    # logging.WARNING
    with caplog.at_level(30):
        _, file_evts = run(
            dicom_path=tmp_path,
            out_dir=tmp_path,
            transfer_syntax=True,
            unique=False,
            zip_single="no",
            convert_palette=True,
            new_uids_needed=False,
            pixel_data_check=False,
        )

    assert set(evt["tag"] for evt in file_evts) == set(
        ["MagneticFieldStrength", "SOPInstanceUID", "Modality", "TransferSyntaxUID"]
    ), "Expected tags not found in file_evts"
    # Check for MagneticFieldStrength event
    assert any(
        evt["tag"] == "MagneticFieldStrength"
        and evt["event"] == "Replace value: b'3000.0' -> b'3.0'"
        for evt in file_evts
    ), "Expected event for MagneticFieldStrength not found."

    # Check for Modality event
    assert any(
        evt["tag"] == "Modality" and evt["event"] == "Replace Modality: None -> OT"
        for evt in file_evts
    ), "Expected event for Modality not found."

    # Check for SOPInstanceUID events
    expected_sop_instance_events = [
        "Replace value: b'1.2.00' -> b'1.2.0'",
        "Replace value: b'1.2.01' -> b'1.2.1'",
        "Replace value: b'1.2.010' -> b'1.2.10'",
        "Replace value: b'1.2.011' -> b'1.2.11'",
        "Replace value: b'1.2.012' -> b'1.2.12'",
        "Replace value: b'1.2.095' -> b'1.2.95'",
        "Replace value: b'1.2.096' -> b'1.2.96'",
        "Replace value: b'1.2.097' -> b'1.2.97'",
        "Replace value: b'1.2.098' -> b'1.2.98'",
        "Replace value: b'1.2.099' -> b'1.2.99'",
    ]

    actual_sop_instance_events = [
        evt["event"]
        for evt in file_evts
        if evt["tag"] == "SOPInstanceUID" and evt["event"] != "...90 more items..."
    ]

    assert all(
        event in actual_sop_instance_events for event in expected_sop_instance_events
    ), "Expected SOPInstanceUID events not found or do not match."

    # Assert on caplog records
    assert len(caplog.records) == 1
    assert caplog.records[0].levelno == 30
    assert caplog.records[0].message == f"test x {num_dcms} across archive"

    # Check the mock call count
    assert mock_generate_and_set_new_uid.call_count == 0

    # Ensure SeriesInstanceUID and StudyInstanceUID are not in file_evts
    assert all(
        evt["tag"] != "SeriesInstanceUID" for evt in file_evts
    ), "SeriesInstanceUID found in file_evts"
    assert all(
        evt["tag"] != "StudyInstanceUID" for evt in file_evts
    ), "StudyInstanceUID found in file_evts"


def test_run_from_zip_validation_mode_1(
    mocker, dcmcoll, tmp_path, raw_data_elem, caplog, config
):
    config.reading_validation_mode = 1
    p_sex = mocker.patch("fw_gear_dicom_fixer.fixers.fix_patient_sex")
    p_sex.side_effect = lambda _: warnings.warn("test")
    num_dcms = 100
    coll = dcmcoll(**{f"test-{i}": {} for i in range(num_dcms)})
    for i, dcm in enumerate(coll):
        tracked_elem1 = raw_data_elem(b"3000.0", tracked=True)
        tracked_elem1._replace(value=b"3.0")
        invalid = f"1.2.0{i}"
        valid = f"1.2.{i}"
        tracked_elem2 = raw_data_elem(
            bytes(invalid, encoding="ascii"),
            tag=(0x0008, 0x0018),
            tracked=True,
            VR="UI",
        )
        tracked_elem2._replace(value=bytes(valid, encoding="ascii"))
        # Mock tracker.track() to return what was passed in
        dcm.read_context.dataelem_dict[hash(tracked_elem1)] = tracked_elem1
        dcm.read_context.dataelem_dict[hash(tracked_elem2)] = tracked_elem2
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll
    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = True
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = False
    # logging.WARNING
    with caplog.at_level(30):
        _, file_evts = run(
            dicom_path=tmp_path,
            out_dir=tmp_path,
            transfer_syntax=True,
            unique=False,
            zip_single="no",
            convert_palette=True,
            new_uids_needed=False,
            pixel_data_check=False,
        )
    # Check for the presence of the expected tags
    assert set(evt["tag"] for evt in file_evts) == set(
        ["MagneticFieldStrength", "SOPInstanceUID", "Modality", "TransferSyntaxUID"]
    ), "Expected tags not found in file_evts"

    # Check for MagneticFieldStrength event
    assert any(
        evt["tag"] == "MagneticFieldStrength"
        and evt["event"] == "Replace value: b'3000.0' -> b'3.0'"
        for evt in file_evts
    ), "Expected event for MagneticFieldStrength not found."

    # Check for Modality event
    assert any(
        evt["tag"] == "Modality" and evt["event"] == "Replace Modality: None -> OT"
        for evt in file_evts
    ), "Expected event for Modality not found."

    # Define expected SOPInstanceUID events, excluding placeholder
    expected_sop_instance_events = [
        "Replace value: b'1.2.00' -> b'1.2.0'",
        "Replace value: b'1.2.01' -> b'1.2.1'",
        "Replace value: b'1.2.010' -> b'1.2.10'",
        "Replace value: b'1.2.011' -> b'1.2.11'",
        "Replace value: b'1.2.012' -> b'1.2.12'",
        "Replace value: b'1.2.095' -> b'1.2.95'",
        "Replace value: b'1.2.096' -> b'1.2.96'",
        "Replace value: b'1.2.097' -> b'1.2.97'",
        "Replace value: b'1.2.098' -> b'1.2.98'",
        "Replace value: b'1.2.099' -> b'1.2.99'",
    ]

    # Gather actual SOPInstanceUID events, excluding placeholder
    actual_sop_instance_events = [
        evt["event"]
        for evt in file_evts
        if evt["tag"] == "SOPInstanceUID" and evt["event"] != "...90 more items..."
    ]

    # Check that expected SOPInstanceUID events are a subset of actual events
    assert all(
        event in actual_sop_instance_events for event in expected_sop_instance_events
    ), "Expected SOPInstanceUID events not found or do not match."

    # Validate the number and content of caplog records
    assert len(caplog.records) == 101, "Unexpected number of caplog records"
    assert (
        caplog.records[0].levelno == 30
    ), "Unexpected logging level in first caplog record"
    assert (
        caplog.records[0].message == f"test x {num_dcms} across archive"
    ), "Unexpected message in first caplog record"


@pytest.mark.parametrize(
    "sops,exp",
    [
        (["1.2.3", "1.2.4", "1.2.4", "1.2.3"], 2),
        (["1.2.3", "1.2.3", "1.2.3", "1.2.3"], 1),
        (["1.2.3", "1.2.4", "1.2.5", "1.2.6"], 4),
    ],
)
def test_run_from_zip_remove_duplicates_zip_single(
    mocker, dcmcoll, tmp_path, sops, exp
):
    fix = mocker.patch("fw_gear_dicom_fixer.fixers.fix_patient_sex")
    fix.return_value = None
    coll = dcmcoll(
        **{f"test-{i}": {"SOPInstanceUID": val} for i, val in enumerate(sops)}
    )
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll
    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = True
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = False
    # logging.WARNING
    out_name, _ = run(
        dicom_path=tmp_path / "test.dcm.zip",
        out_dir=tmp_path,
        transfer_syntax=True,
        unique=True,
        zip_single="yes",
        convert_palette=True,
        new_uids_needed=False,
        pixel_data_check=False,
    )
    dcms = dicom.DICOMCollection.from_zip(tmp_path / "test.dcm.zip")
    assert len(dcms) == exp
    assert out_name == "test.dcm.zip"


@pytest.mark.parametrize(
    "in_file, dcm_len, zip_single, exp",
    [
        ("test.dcm", 1, "yes", "test.dcm.zip"),
        ("test.dcm", 1, "no", "test.dcm"),
        ("test.dcm", 1, "match", "test.dcm"),
        ("test.dcm.zip", 1, "yes", "test.dcm.zip"),
        ("test.dcm.zip", 1, "no", "test.dcm"),
        ("test.dcm.zip", 1, "match", "test.dcm.zip"),
        # If len > 1, all outputs should be .zip, no matter input, or zip_single
        ("test.dcm", 2, "yes", "test.dcm.zip"),
        ("test.dcm", 2, "no", "test.dcm.zip"),
        ("test.dcm", 2, "match", "test.dcm.zip"),
        ("test.dcm.zip", 2, "yes", "test.dcm.zip"),
        ("test.dcm.zip", 2, "no", "test.dcm.zip"),
        ("test.dcm.zip", 2, "match", "test.dcm.zip"),
    ],
)
def test_get_output_filename(in_file, dcm_len, zip_single, exp):
    dcms = MagicMock()
    dcms.__len__.return_value = dcm_len
    in_file = Path(in_file)
    assert get_output_filename(in_file, dcms, zip_single) == exp


def test_run_from_zip_remove_duplicates_not_zip_single(mocker, dcmcoll, tmp_path):
    sops = ["1.2.3", "1.2.3", "1.2.3", "1.2.3"]
    fix = mocker.patch("fw_gear_dicom_fixer.fixers.fix_patient_sex")
    fix.return_value = None
    coll = dcmcoll(
        **{f"test-{i}": {"SOPInstanceUID": val} for i, val in enumerate(sops)}
    )
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll
    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = True
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = False
    # logging.WARNING
    out_name, _ = run(
        dicom_path=tmp_path / "test.dcm.zip",
        out_dir=tmp_path,
        transfer_syntax=True,
        unique=True,
        zip_single="no",
        convert_palette=True,
        new_uids_needed=False,
        pixel_data_check=False,
    )
    out = tmp_path / "test.dcm"
    assert out_name == "test.dcm"
    assert out.exists()
    assert out.is_file()


def test_run_from_single_dcm_zip_single(mocker, dcmcoll, tmp_path, raw_data_elem):
    coll = dcmcoll(**{"test1": {}})
    tracked_elem = raw_data_elem(b"3000.0", tracked=True)
    tracked_elem._replace(value=b"3.0")
    for dcm in coll:
        dcm.read_context.dataelem_dict[hash(tracked_elem)] = tracked_elem

    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = False
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = True
    dcm_coll_new = mocker.patch.object(dicom.DICOMCollection, "__new__")
    dcm_coll_init = mocker.patch.object(dicom.DICOMCollection, "__init__")
    dcm_coll_new.return_value = coll
    dcm_coll_init.return_value = None
    _, evts = run(
        dicom_path=tmp_path / "test.dcm.zip",
        out_dir=tmp_path,
        transfer_syntax=True,
        unique=True,
        zip_single="yes",
        convert_palette=True,
        new_uids_needed=False,
        pixel_data_check=False,
    )

    assert len(evts) == 3
    # Check for the presence of the expected tags in evts
    assert sorted([evt["tag"] for evt in evts]) == sorted(
        ["MagneticFieldStrength", "Modality", "TransferSyntaxUID"]
    ), "Expected tags not found in evts"

    assert any(
        evt["tag"] == "MagneticFieldStrength"
        and evt["event"] == "Replace value: b'3000.0' -> b'3.0'"
        for evt in evts
    ), "Expected event for MagneticFieldStrength not found."

    assert any(
        evt["tag"] == "Modality" and evt["event"] == "Replace Modality: None -> OT"
        for evt in evts
    ), "Expected event for Modality not found."

    assert any(
        evt["tag"] == "TransferSyntaxUID"
        and "Replace Implicit VR Little Endian -> Explicit VR Little Endian"
        in evt["event"]
        for evt in evts
    ), "Expected event for TransferSyntaxUID not found."

    dcms = dicom.DICOMCollection.from_zip(tmp_path / "test.dcm.zip")
    assert len(dcms) == 1, "Unexpected number of DICOM files in collection"


def test_run_from_single_dcm_not_zip_single(mocker, dcmcoll, tmp_path, raw_data_elem):
    coll = dcmcoll(**{"test1": {}})
    tracked_elem = raw_data_elem(b"3000.0", tracked=True)
    tracked_elem._replace(value=b"3.0")
    for dcm in coll:
        dcm.read_context.dataelem_dict[hash(tracked_elem)] = tracked_elem

    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = False
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = True
    dcm_coll_new = mocker.patch.object(dicom.DICOMCollection, "__new__")
    dcm_coll_init = mocker.patch.object(dicom.DICOMCollection, "__init__")
    dcm_coll_new.return_value = coll
    dcm_coll_init.return_value = None
    _, evts = run(
        dicom_path=tmp_path / "test.dcm.zip",
        out_dir=tmp_path,
        transfer_syntax=True,
        unique=True,
        zip_single="no",
        convert_palette=True,
        new_uids_needed=False,
        pixel_data_check=False,
    )

    assert len(evts) == 3
    assert sorted([evt["tag"] for evt in evts]) == sorted(
        ["MagneticFieldStrength", "Modality", "TransferSyntaxUID"]
    ), "Expected tags not found in evts"

    assert any(
        evt["tag"] == "MagneticFieldStrength"
        and evt["event"] == "Replace value: b'3000.0' -> b'3.0'"
        for evt in evts
    ), "Expected event for MagneticFieldStrength not found."

    assert any(
        evt["tag"] == "Modality" and evt["event"] == "Replace Modality: None -> OT"
        for evt in evts
    ), "Expected event for Modality not found."

    assert any(
        evt["tag"] == "TransferSyntaxUID"
        and "Replace Implicit VR Little Endian -> Explicit VR Little Endian"
        in evt["event"]
        for evt in evts
    ), "Expected event for TransferSyntaxUID not found."

    out = tmp_path / "test.dcm"
    assert out.exists()
    assert out.is_file()


@pytest.mark.parametrize("zip_single", ["yes", "no"])
def test_run_write_errors_no_output(
    dcmcoll, mocker, tmp_path, raw_data_elem, caplog, zip_single
):
    coll = dcmcoll(**{"test1": {}})
    save = mocker.patch.object(coll[0].dataset.raw, "save_as")
    save.side_effect = ValueError
    tracker = MagicMock()
    tracked_elem = raw_data_elem(b"3000.0", tracked=True)
    tracked_elem._replace(value=b"3.0")
    # Mock tracker.track() to return what was passed in
    tracker.track.side_effect = lambda val: val
    tracker.data_elements = [tracked_elem]
    object.__setattr__(coll[0], "tracker", tracker)

    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = False
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = True
    dcm_coll_new = mocker.patch.object(dicom.DICOMCollection, "__new__")
    dcm_coll_init = mocker.patch.object(dicom.DICOMCollection, "__init__")
    dcm_coll_new.return_value = coll
    dcm_coll_init.return_value = None
    with caplog.at_level(10):
        _, evts = run(
            dicom_path=tmp_path / "test.dcm.zip",
            out_dir=tmp_path,
            transfer_syntax=True,
            unique=True,
            zip_single=zip_single,
            convert_palette=True,
            new_uids_needed=False,
            pixel_data_check=False,
        )
    assert "Got exception saving dicom(s)" in caplog.record_tuples[-1][2]
    assert evts is None
    assert not (tmp_path / "test.dcm.zip").exists()


def test_run_gear_fail(dcmcoll, mocker, tmp_path):
    coll = dcmcoll(**{"test1": {}})

    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = True
    dcm_coll_new = mocker.patch.object(dicom.DICOMCollection, "__new__")
    dcm_coll_init = mocker.patch.object(dicom.DICOMCollection, "__init__")
    dcm_coll_new.return_value = coll
    dcm_coll_init.return_value = None

    standardize_transfer_syntax = mocker.patch(
        "fw_gear_dicom_fixer.main.standardize_transfer_syntax"
    )
    standardize_transfer_syntax.side_effect = AttributeError()
    pixel_check = mocker.patch("fw_file.dicom.DICOM")
    pixel_check.side_effect = Exception()

    _, evts = run(
        dicom_path=tmp_path / "test.dcm.zip",
        out_dir=tmp_path,
        transfer_syntax=True,
        unique=False,
        zip_single="no",
        convert_palette=True,
        new_uids_needed=False,
        pixel_data_check=True,
    )

    expected_gear_fail_events = [
        "Decompression failed due to large file size",
        "Pixel data unparsable",
    ]

    actual_gear_fail_events = [
        evt["event"] for evt in evts if evt["tag"] == "Gear Fail"
    ]

    assert sorted(actual_gear_fail_events) == sorted(
        expected_gear_fail_events
    ), "Expected 'Gear Fail' events not found or do not match."


def test_run_DICOMCollection_EOFError(dcmcoll, mocker, tmp_path, config):
    # If an EOFError is raised during .from_zip(), continue and log/add qc event
    config.reading_validation_mode = 2
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = False
    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = True
    num_dcms = 10
    coll = dcmcoll(**{f"test-{i}": {} for i in range(num_dcms)})
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.side_effect = [EOFError, coll]
    _, file_evts = run(
        dicom_path=tmp_path,
        out_dir=tmp_path,
        transfer_syntax=False,
        unique=False,
        zip_single="no",
        convert_palette=False,
        new_uids_needed=False,
        pixel_data_check=False,
    )

    # Check for the "Sequence Delimiter" event
    assert any(
        evt["tag"] == "Sequence Delimiter"
        and evt["event"] == "Missing delimiter (FFFE, E0DD) added."
        for evt in file_evts
    ), "Expected 'Sequence Delimiter' event not found."


def test_run_DICOMCollection_other_exception(mocker, tmp_path, config):
    # If a non-EOF exception is raised during .from_zip() and strict
    # validation is on, log and fail.
    config.reading_validation_mode = 2
    sniff_dcm = mocker.patch("fw_gear_dicom_fixer.main.sniff_dcm")
    sniff_dcm.return_value = False
    is_zipfile = mocker.patch("zipfile.is_zipfile")
    is_zipfile.return_value = True
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.side_effect = [BaseException("Something went wrong.")]
    with pytest.raises(BaseException):
        run(
            dicom_path=tmp_path,
            out_dir=tmp_path,
            transfer_syntax=False,
            unique=False,
            zip_single="no",
            convert_palette=False,
            new_uids_needed=False,
            pixel_data_check=False,
        )
